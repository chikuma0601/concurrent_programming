#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

/*
 *  no_child_wait.c
 *  Created by Chikuma, 04/04/2020
 *
 *  This program is used to play with wait() function and inspect its behavior
 *  when there are no child processes.
 */
int main (int argc, char * argv[])
{
    char buf[100];
    int status = 1;
    pid_t pid;

    sprintf(buf, "No child wait starting...\n");
    write(1, buf, strlen(buf));

    pid = wait(&status);

    /* wait() will return -1 due to there are no child processes */
    /* parameter "status" will remain the same value */
    printf("child terminate status = %d\n", status);
    printf("pid = %d\n", pid);

    return 0;
}
