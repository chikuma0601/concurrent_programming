#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
 * grandson_process.c
 * Created by Chikuma, 04/04/2020
 *
 * Some experimemt of UNIX processes, inspect when a child process terminates,
 * what will the "grandson process"'s parent process be? Will it be init or its
 * "grandparent process"?
 */

 /* Conclusion:
  *     grandson's ppid will be 1 (init process pid).
  *     Kind of understandable, due to that shell also forks processes and if
  *     the processes terminates before child terminates, the child pid will
  *     also become 1.
  */
int main (int argc, char * argv[])
{
    char buf[100];
    pid_t child, waitPid;
    int status;

    sprintf(buf, "The parent process pid = %d\n", getpid());
    write(1, buf, strlen(buf));

    if ((child = fork()) == 0) {
        /* child stuff, fork a child and terminate */
        sprintf(buf, "\tChild process pid = %d, parent = %d\n", getpid(), getppid());
        write(1, buf, strlen(buf));

        if ((child = fork()) == 0) {
            /* grandson stuff, sleeps and print stuffs */
            sprintf(buf, "\t\tGrandson process pid = %d, parent = %d\n",
                    getpid(),
                    getppid());
            write(1, buf, strlen(buf));

            sleep(5);   /* make sure that child process terminates */

            sprintf(buf, "\t\tGrandson process pid = %d, parent = %d\n",
                    getpid(),
                    getppid());
            write(1, buf, strlen(buf));
            sprintf(buf, "--grandson terminate\n");
            write(1, buf, strlen(buf));
            exit(0);
        }

        sleep(2);
        sprintf(buf, "--child terminate\n");
        write(1, buf, strlen(buf));
        exit(0);
    }

    /* make sure that this terminates after granson terminates */
    sleep(10);


    /* see if main process can wait grandson process */
    while((waitPid = wait(&status)) != -1) {
        sprintf(buf, "Waited pid = %d\n", waitPid);
        write(1, buf, strlen(buf));
    }

    sprintf(buf, "--parent terminate\n");
    write(1, buf, strlen(buf));


    return 0;
}
