#ifndef MERGE_SORT_H
#define MERGE_SORT_H

void swap(int * a, int * b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void printArray(int * array, int size)
{
    int i;
    printf("[ ");
    for (i = 0; i < size; ++i)
        printf("%d ", array[i]);
    printf("]\n");
}

void merge(int * array, int lowerBound, int middle, int upperBound)
{
    /* thonk... */
    int lowerIndex = lowerBound;
    int upperIndex = middle + 1;
    if (array[middle] <= array[upperIndex])
        return;

    while (lowerIndex <= middle && upperIndex <= upperBound) {
        printArray(array, upperIndex + 1);
        printf("lower: %d, upper: %d\n", lowerIndex, upperIndex);
        if (array[lowerIndex] <= array[upperIndex]) {
            ++lowerIndex;
        }
        else {
            int value = array[upperIndex];
            int index = upperIndex;
            while (index != lowerIndex) {
                array[index] = array[index - 1];
                --index;
            }
            array[lowerIndex] = value;
            ++lowerIndex;
            ++middle;
            upperIndex += 1;
        }
    }
}

void mergeSort(int * array, int lowerBound, int upperBound)
{
    int middle;
    /* recursion terminate condition */
    if (lowerBound == upperBound)
        return;
    else if (upperBound - lowerBound == 1) {
        //swap(&array[upperBound], &array[lowerBound]);
        return;
    }
    /* splitting... */
    middle = (lowerBound + upperBound) / 2;
    mergeSort(array, lowerBound, middle);
    mergeSort(array, middle + 1, upperBound);
    /* merge */
    merge(array, lowerBound, middle, upperBound);
}

#endif
