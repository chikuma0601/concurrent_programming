#include "../src/merge_sort.h"

TEST(MergeSort, swap) {
    int a[] = {1, 2, 3, 4, 5};
    swap(&a[0], &a[1]);
    ASSERT_EQ(2, a[0]);
    ASSERT_EQ(1, a[1]);
}

TEST(MergeSort, merge) {
    int a[] = {4, 6, 3, 5};
    merge(a, 0, 1, 3);  /* a, lower, middle, upper */
    ASSERT_EQ(3, a[0]);
    ASSERT_EQ(4, a[1]);
    ASSERT_EQ(5, a[2]);
    ASSERT_EQ(6, a[3]);
}

TEST(MergeSort, mergeSort) {
    int a[] = {1,3,7,4,1,6,8,7,3,1,5,7,4,5};
    mergeSort(a, 0, 14);  /* a, lower, middle, upper */
    printArray(a, 14);
}
