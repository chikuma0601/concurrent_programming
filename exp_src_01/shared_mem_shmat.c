#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

struct DataStruct {
    int     a;
    double  b;
    char    c;
};


struct DataStruct * attachToMemory(int id)
{
    struct DataStruct * shmPtr;
    shmPtr = (struct DataStruct *) shmat(id, NULL, 0);
    if ((void*)shmPtr < 0) {
        printf("An error occured when attaching to a shared memory...\n");
        exit(1);
    }
    return shmPtr;
}

void writeToShm(struct DataStruct* shmPtr)
{
    printf("Writing to Shm...\n");
    shmPtr->a = 1;
    shmPtr->b = 1.141;
    shmPtr->c = 'b';
    printf("End writing to shm.\n");
}

void printShmContent(struct DataStruct* shmPtr)
{
    char buf[100];
    sprintf(buf,
            "Data in shm: a=%d, b=%f, c=%c\n",
            shmPtr->a,
            shmPtr->b,
            shmPtr->c);
    write(1, buf, strlen(buf));
}

int main (int argc, char * argv[])
{
    char buf[100];
    pid_t pid;
    key_t shmKey;
    int shmId;
    struct DataStruct *shmPtr;

    sprintf(buf, "TESTING SHMAT\n");
    write(1, buf, strlen(buf));

    shmKey = ftok("./", 'a');
    shmId = shmget(
        shmKey,
        sizeof(struct DataStruct),
        IPC_CREAT | 0666
    );

    /* if failed to create shm */
    if (shmId < 0) {
        sprintf(buf, "failed to create a shared memory, exit...\n");
        write(1, buf, strlen(buf));
        exit(1);
    }

    /* attach */
    shmPtr = attachToMemory(shmId);

    /* show some shm infos */
    printf("--Shared Memory Segments Info:\n");
    printf("\tShmKey: %d\n", shmKey);
    printf("\tShmID : %d\n", shmId);
    printf("\tShmPtr: %p\n", (void*)shmPtr);

    /* write to shm and print out the data */
    writeToShm(shmPtr);
    printShmContent(shmPtr);

    return 0;
}
