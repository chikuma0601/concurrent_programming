#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LIM (30)

int main (int argc, char * argv[])
{
	srand(time(0));

	/* repeat LIM times */
	for (int i = 0; i < LIM; ++i)
	{
		int randomNum = rand() / 10;
		for (int j = 0; j < randomNum; ++j) {}
		printf("The generated number of progA is: %d\n", randomNum);
	}

	return 0;
}
