#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main (int argc, char * argv[])
{
    char printBuffer[1024];
    key_t key_1 = ftok("./", '0');
    key_t key_2 = ftok("./", '1');
    int shmId1, shmId2;

    sprintf(printBuffer, "*** MAIN: shared memory key = %d\n", key_1);
    write(1, printBuffer, strlen(printBuffer));
    sprintf(printBuffer, "*** MAIN: shared memory key = %d\n", key_2);
    write(1, printBuffer, strlen(printBuffer));
    /*
    shmId1 = shmget(key_1, sizeof(int) * 8, IPC_CREAT | 0666);
    sprintf(printBuffer, "*** MAIN: shared memory created\n");
    write(1, printBuffer, strlen(printBuffer));
    */
    shmId2 = shmget(key_2, sizeof(int) * 8, IPC_CREAT | 0666);
    sprintf(printBuffer, "*** MAIN: shared memory created\n");
    write(1, printBuffer, strlen(printBuffer));

    /*printf("ID 1 %d\n", shmId1);*/
    printf("ID 2 %d\n", shmId2);

    return 0;
}
