#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main (int argc, char *argv[])
{
    pid_t pid;
    char  buffer[100];


    pid = fork();
    if (pid < 0) {
        printf("error occured when creating a child process\n");
        exit(1);
    }
    else if (pid == 0) {    /* child process */
        sprintf(buffer, "\tFrom child=%d, parent=%d\n", getpid(), getppid());
        write(1, buffer, strlen(buffer));
        sleep(5);
        sprintf(buffer, "\tFrom child=%d, parent=%d 5 secs later\n", getpid(), getppid());
        write(1, buffer, strlen(buffer));
    }
    else {                  /* parent process */
        sprintf(buffer, "From child=%d, parent=%d\n", getpid(), getppid());
        write(1, buffer, strlen(buffer));
        sleep(2);
        sprintf(buffer, "From parent, Done!\n");
        write(1, buffer, strlen(buffer));
    }

    return 0;
}
