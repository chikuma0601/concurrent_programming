#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define RUN_LIM 200

int main (int argc, char * argv[])
{
    pid_t   myId, parentId, forkCheckId;
    int     i;
    char    buffer[100];
    char    parentChildIndicator[100];

    forkCheckId = fork();
    if (forkCheckId < 0) {
        printf("An error occurend when creating a child process.\n");
        exit(1);
    }
    else if (forkCheckId == 0) {
        strncpy(parentChildIndicator, "This is child\t", 100);
    }
    else {
        strncpy(parentChildIndicator, "This is parent\t", 100);
    }


    myId = getpid();
    parentId = getppid();

    for (i = 0; i < RUN_LIM; ++i) {
        sprintf(buffer, "%sFrom myId=%ld,\tparentId=%ld,\ti=%d\n",
                parentChildIndicator,
                myId,
                parentId,
                i);
        write(1, buffer, strlen(buffer));
    }

    return 0;
}
