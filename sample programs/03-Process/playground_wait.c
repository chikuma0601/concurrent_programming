#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void child() {
    /* do stuffs as a child process */
    return;
}

void parent() {
    int i;
    for (i = 0; i < 100; ++i)
        ;
}

int main(int argc, char *argv[])
{
    pid_t   pid, pid_child;
    /* if status is not null,
     * status of child process will be stored in this int
     */
    int     status;

    if ((pid = fork() == 0)) {
        child();
        exit(1);        /* status will be 256 */
    }
    else {
        parent();
        pid_child = wait(&status);
    }

    printf("pid_child is %d\n", pid_child);
    printf("wait status is %d\n", status);
    return 0;
}
