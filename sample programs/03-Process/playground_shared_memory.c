#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>            /* ipc = inter process communication */
#include <sys/shm.h>            /* XSI shared memeory facility */

struct DataStruct {
    int     a;
    double  b;
    char    c;
};

void child(struct DataStruct *shmPtr)
{
    char printBuffer[100];
    /* do some child process stuffs */
    sprintf(printBuffer, "This is child process\n");
    write(1, printBuffer, strlen(printBuffer));

    sprintf(printBuffer,
            "Data in shm: a=%d, b=%f, c=%c\n",
            shmPtr->a,
            shmPtr->b,
            shmPtr->c);
    write(1, printBuffer, strlen(printBuffer));
}

int main (int argc, char * argv[])
{
    key_t   shmKey;
    int     shmId;
    struct DataStruct    *shmPtr;

    pid_t   childPid;

    shmKey = ftok("./", 'x');   /* create a key */
    shmId = shmget(
        shmKey,
        sizeof(struct DataStruct),
        IPC_CREAT | 0666        /* IPC_CREAT is defined as 512 in decimal in sys/ipc.h */
    );

    if (shmId < 0) {
        printf("failed to create a shared memory, exit...\n");
        exit(1);
    }

    shmPtr = (struct DataStruct *) shmat(shmId, NULL, 0);
    if ((int)shmPtr < 0) {
        printf("An error occured when attaching to a shared memory...\n");
        exit(1);
    }

    /* write some stuffs to shared memory */
    shmPtr->a = 10;
    shmPtr->b = 1.141;
    shmPtr->c = "a";

    if ((childPid = fork()) < 0) {
        printf("An error occured when creating a child process.\n");
        exit(1);
    }
    else if (childPid == 0) {
        child(shmPtr);
        exit(0);
    }

    int childTerminateStatus;
    int checkPid = wait(&childTerminateStatus);

    /* detach and delete the shared memory */
    shmdt((void *)shmPtr);
    /*shmctl(shmId, IPC_RMID, NULL);*/
    printf("Detached and deleted the shared memory\n");

    return 0;
}
