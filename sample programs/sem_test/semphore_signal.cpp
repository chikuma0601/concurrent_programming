#include <iostream>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>

sem_t s1, s2, s3;

void * p1 (void *) {
    while(1) {
        sem_wait(&s1);
        std::cout << 1 << std::endl;
        sem_post(&s2);
    }
}

void * p2 (void *) {
    while(1) {
        sem_wait(&s2);
        std::cout << 2 << std::endl;
        sem_post(&s3);
        sem_wait(&s2);
        std::cout << 2 << std::endl;
        sem_post(&s1);
    }
}

void * p3 (void *) {
    while(1) {
        sem_wait(&s3);
        std::cout << 3 << std::endl;
        sem_post(&s2);
    }
}


int main (int argc, char * argv[]) {
    pthread_t t1;
    pthread_t t2;
    pthread_t t3;

    sem_init(&s1, 0, 1);
    sem_init(&s2, 0, 0);
    sem_init(&s3, 0, 0);

    pthread_create(&t1, NULL, &p1, NULL);
    pthread_create(&t2, NULL, &p2, NULL);
    pthread_create(&t3, NULL, &p3, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);

    return 0;
}
