#include  <stdio.h>
#include  <stdlib.h>

int  main(void)
{
     int   i, LIMIT;
     char  input[100];

     void * readCheck = fgets(input, 100, stdin);   // read a complete input line
     if (!readCheck) {
         printf("Something went wrong when reading...exiting...\n");
         exit(1);
     }
     LIMIT = atoi(input); // convert to integer
     for (i = 1; i <= LIMIT; i++) {  // repeat
          if (fgets(input, 100, stdin) != NULL)   // read a complete input line
              printf("     From B: %s", input);
          else
              printf("An error occured when reading from input\n");
     }
}
