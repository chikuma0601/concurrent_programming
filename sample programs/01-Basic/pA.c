#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
     int   i, LIMIT;
     char  output[100];

     if (argc < 2) {
         printf("need one more argument to do anything :(\n");
         return 1;
     }

     LIMIT = atoi(argv[1]);  // read command line argument, atoi = const * char to int
     printf("line(s) to print: %d\n", LIMIT);  // print its value
     for (i = 1; i <= LIMIT; i++) {  // iterate
          sprintf(output, "Printing %d from A", i);
          printf("%s\n", output);
     }
}
