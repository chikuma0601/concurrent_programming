/* ----------------------------------------------------------- */
/* NAME : 簡少澤                             User ID: 105820049 */
/* DUE DATE : 03/30/2020                                       */
/* PROGRAM ASSIGNMENT # 1                                      */
/* FILE NAME : prog1.c                                         */
/* PROGRAM PURPOSE :                                           */
/*    forks 4 child processes and execute different            */
/*    calculations.                                            */
/* ----------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

typedef int bool;
#define true     1
#define false    0

/* ----------------------------------------------------------- */
/* FUNCTION  fibonacci :                                       */
/*     Workhorse of fibonacci, calcuates fib(n).               */
/* PARAMETER USAGE :                                           */
/*    n - get nth fibonacci number                             */
/* FUNCTION CALLED :                                           */
/*    fibonacci                                                */
/* ----------------------------------------------------------- */
long fibonacci(int n)
{
    if (n == 1 || n == 2)
        return 1;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

/* ----------------------------------------------------------- */
/* FUNCTION  computeFib :                                      */
/*     Child process computeFib. Compute and prints fib(n)     */
/* PARAMETER USAGE :                                           */
/*    n - get nth fibonacci number                             */
/* FUNCTION CALLED :                                           */
/*    fibonacci                                                */
/* ----------------------------------------------------------- */
void computeFib(int n)
{
    char buf[100];
    sprintf(buf, "   Fibonacci Process Started\n");
    write(1, buf, strlen(buf));
    sprintf(buf, "   Input Number %d\n", n);
    write(1, buf, strlen(buf));
    sprintf(buf, "   Fibonacci Number f(%d) is %ld\n", n, fibonacci(n));
    write(1, buf, strlen(buf));
    sprintf(buf, "   Fibonacci Process Exits\n");
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  generateRand :                                    */
/*     Generates a random number in range [0, 1)               */
/* PARAMETER USAGE :                                           */
/*    (None)                                                   */
/* FUNCTION CALLED :                                           */
/*    (None)                                                   */
/* ----------------------------------------------------------- */
double generateRand()
{
    return ((float) rand()) / RAND_MAX;
}

/* ----------------------------------------------------------- */
/* FUNCTION  throwNeedle :                                     */
/*     Simulate throwing a needle, if the needle crosses a     */
/*     line, returns true. Else returns false.                 */
/* PARAMETER USAGE :                                           */
/*    (None)                                                   */
/* FUNCTION CALLED :                                           */
/*    generateRand                                             */
/* ----------------------------------------------------------- */
bool throwNeedle()
{
    double d = generateRand();
    double a = generateRand() * 2 * acos(-1.0);
    double throwResult = d + 1 * sin(a);
    if (throwResult < 0 || throwResult > 1)
        return true;
    return false;
}

/* ----------------------------------------------------------- */
/* FUNCTION  buffonNeedle :                                    */
/*     Child process buffonNeedle. Simulates the probability   */
/*     of Buffon's Needle problem.                             */
/* PARAMETER USAGE :                                           */
/*    r - The times of experiment(throwing a needle) be done.  */
/* FUNCTION CALLED :                                           */
/*    throwNeedle                                              */
/* ----------------------------------------------------------- */
void buffonNeedle(int r)
{
    char buf[100];
    sprintf(buf, "      Buffon's Needle Process Started\n");
    write(1, buf, strlen(buf));
    sprintf(buf, "      Input Number %d\n", r);
    write(1, buf, strlen(buf));

    int i, crossCount = 0;
    for (i = 0; i < r; ++i) {   /* throw r times */
        if (throwNeedle())
            ++crossCount;
    }

    sprintf(buf, "      Estimated Probability is %.5f\n", (double)crossCount / r);
    write(1, buf, strlen(buf));
    sprintf(buf, "      Buffon's Needle Process Exits\n");
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  inEllipse :                                       */
/*     Generates random x < a and y < b an see if this point   */
/*     falls in the ellipse with specified a and b.            */
/* PARAMETR USAGE :                                            */
/*    a - semi-major axis length                               */
/*    b - semi-minor axis length                               */
/* FUNCTION CALLED :                                           */
/*    generateRand                                             */
/* ----------------------------------------------------------- */
bool inEllipse(int a, int b)
{
    double x = generateRand() * a;
    double y = generateRand() * b;
    if (((x * x) / (a * a) + (y * y) / (b * b)) <= 1)
        return true;
    return false;
}

/* ----------------------------------------------------------- */
/* FUNCTION  ellipse :                                         */
/*     Child process of ellipse area. Calculates the area of a */
/*     given ellipse.                                          */
/* PARAMETER USAGE :                                           */
/*    s - points to be randomly picked                         */
/*    a - semi-major axis length                               */
/*    b - semi-minor axis length                               */
/* FUNCTION CALLED :                                           */
/*    inEllipse                                                */
/* ----------------------------------------------------------- */
void ellipse(int a, int b, int s)
{
    char buf[100];
    sprintf(buf, "         Ellipse Area Process Started\n");
    write(1, buf, strlen(buf));
    sprintf(buf, "         Total random Number Pairs %d\n", s);
    write(1, buf, strlen(buf));
    sprintf(buf, "         Semi-Major Axis Length %d\n", a);
    write(1, buf, strlen(buf));
    sprintf(buf, "         Semi-Minor Axis Length %d\n", b);
    write(1, buf, strlen(buf));

    int i, t = 0;
    for (i = 0; i < s; ++i) {
        if (inEllipse(a, b))
            ++t;
    }

    sprintf(buf, "         Total Hits %d\n", t);
    write(1, buf, strlen(buf));

    double estArea = 4 * ((double)t / s) * a * b;
    double actArea = acos(-1.0) * a * b;

    sprintf(buf, "         Estimated Area is %.5f\n", estArea);
    write(1, buf, strlen(buf));
    sprintf(buf, "         Actual Area is %.5f\n", actArea);
    write(1, buf, strlen(buf));
    sprintf(buf, "         Ellipse Area Process Exits\n");
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  dropBall :                                        */
/*     Simulates a ball dropped in a pinball game.             */
/* PARAMETER USAGE :                                           */
/*    x - The number of bins in this game.                     */
/* FUNCTION CALLED :                                           */
/*    generateRand                                             */
/* ----------------------------------------------------------- */
int dropBall(int x)
{
    int i, bin = 0;                 /* bin is numbered from 0 to x - 1 */
    for (i = 0; i < x - 1; ++i) {
        if (generateRand() < 0.5)   /* P of 0.5, the ball will go right */
            ++bin;
    }
    return bin;
}

/* ----------------------------------------------------------- */
/* FUNCTION  binIndexWithMaxBall :                             */
/*     Get the index of an array that contains the largest     */
/*     number.                                                 */
/* PARAMETER USAGE :                                           */
/*    bins - The array(bins)                                   */
/*    x - The number of bins in this game.                     */
/* FUNCTION CALLED :                                           */
/*    (None)                                                   */
/* ----------------------------------------------------------- */
int binIndexWithMaxBall(int * bins, int x)
{
    int i, max = 0, maxIndex = -1;
    for (i = 0; i < x; ++i) {
        if (bins[i] > max) {
            max = bins[i];
            maxIndex = i;
        }
    }
    return maxIndex;
}

/* ----------------------------------------------------------- */
/* FUNCTION  generateAsterisks :                               */
/*     Get the asterisks(percentage) of a bin compare to max   */
/*     bin.                                                    */
/* PARAMETER USAGE :                                           */
/*    outStr - string for results, needs to have len >= 51     */
/*    outLen - length of output string.                        */
/*    ball - balls in a bin                                    */
/*    maxBall - balls in the bin that has maximum              */
/* FUNCTION CALLED :                                           */
/*    (None)                                                   */
/* ----------------------------------------------------------- */
void generateAsterisks(char * outStr, int outLen, int ball, int maxBall)
{
    if (outLen < 51) {
        char buf[100];
        sprintf(buf, "[ERR] length of the output string needs 51.\n");
        write(1, buf, strlen(buf));
        return;
    }
    int i;
    char asteriskBar[51] = "";                /* maximum of 50 asterisks */
    for (i = 0; i < (double)ball / maxBall * 50; ++i) {
        strncat(asteriskBar, "*", 1);
    }
    strncpy(outStr, asteriskBar, outLen);
}

/* ----------------------------------------------------------- */
/* FUNCTION  printPinBallResult :                              */
/*     Prints the bin index, number of balls, percentage and   */
/*     good looking asterisks.                                 */
/* PARAMETER USAGE :                                           */
/*    bins - The array of the result of the pinball game.      */
/*    x - The number of bins in this game.                     */
/*    y - The number of balls be dropped.                      */
/* FUNCTION CALLED :                                           */
/*    binIndexWithMaxBall                                      */
/*    generateAsterisks                                        */
/* ----------------------------------------------------------- */
void printPinBallResult(int * bins, int x, int y)
{
    int i, maxIndex;
    char buf[100], asteriskBar[51];
    maxIndex = binIndexWithMaxBall(bins, x);
    for (i = 0; i < x; ++i) {
        generateAsterisks(asteriskBar,
                          51,
                          bins[i],
                          bins[maxIndex]);
        sprintf(buf,
                "%3d-(%7d)-(%5.2f%%)|%s\n",
                i + 1,
                bins[i],
                (double)bins[i] / y * 100,
                asteriskBar
                );
        write(1, buf, strlen(buf));
    }
}

/* ----------------------------------------------------------- */
/* FUNCTION  pinballGame :                                     */
/*     Child process for pinball game problem.                 */
/* PARAMETER USAGE :                                           */
/*    x - The number of bins in this game.                     */
/*    y - The number of balls be dropped.                      */
/* FUNCTION CALLED :                                           */
/*    dropBall, printPinBallResult                             */
/* ----------------------------------------------------------- */
void pinballGame(int x, int y)
{
    int i;
    char buf[100];

    /* print some happy messages */
    sprintf(buf, "Simple Pinball Process Started\n");
    write(1, buf, strlen(buf));
    sprintf(buf, "Number of Bins %d\n", x);
    write(1, buf, strlen(buf));
    sprintf(buf, "Number of Ball Droppings %d\n", y);
    write(1, buf, strlen(buf));

    int * bins = malloc(sizeof(int) * x);
    if (bins == NULL) {                     /* allocation failed */
        sprintf(buf, "failed to allocate memory for bins...\n");
        write(1, buf, strlen(buf));
        exit(1);
    }

    for (i = 0; i < x; ++i)                 /* init all elements to 0 */
        bins[i] = 0;

    for (i = 0; i < y; ++i) {               /* drop y balls */
        bins[dropBall(x)] += 1;
    }

    printPinBallResult(bins, x, y);
    free(bins);

    sprintf(buf, "Simple Pinball Process Exits\n");
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  main :                                            */
/*     main program. Creats other child processes.             */
/* PARAMETER USAGE :                                           */
/*    argc - argc                                              */
/*    argv - argv                                              */
/* FUNCTION CALLED :                                           */
/*    computeFib, buffonNeedle, ellipse, pinballGame           */
/* ----------------------------------------------------------- */
int main (int argc, char * argv[])
{
    srand(time(NULL));              /* setup rand */
    pid_t pid1, pid2, pid3, pid4;
    int i, status;
    char buf[100];

    sprintf(buf, "Main Process Started\n");
    write(1, buf, strlen(buf));

    if (argc < 8) {
        sprintf(buf, "Missing some arguments, exiting...\n");
        write(1, buf, strlen(buf));
        exit(1);
    }

    sprintf(buf, "Fibonacci Input            = %s\n", argv[1]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Buffon's Needle Iterations = %s\n", argv[2]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Total random Number Pairs  = %s\n", argv[3]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Semi-Major Axis Length     = %s\n", argv[4]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Semi-Minor Axis Length     = %s\n", argv[5]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Number of Bins             = %s\n", argv[6]);
    write(1, buf, strlen(buf));
    sprintf(buf, "Number of Ball Droppings   = %s\n\n", argv[7]);
    write(1, buf, strlen(buf));

    if ((pid1 = fork()) == 0) {
        sprintf(buf, "Fibonacci Process Created\n");
        write(1, buf, strlen(buf));
        computeFib(atoi(argv[1]));
        exit(0);
    }

    if ((pid2 = fork()) == 0) {
        sprintf(buf, "Buffon's Needle Process Created\n");
        write(1, buf, strlen(buf));
        buffonNeedle(atoi(argv[2]));
        exit(0);
    }

    if ((pid3 = fork()) == 0) {
        sprintf(buf, "Ellipse Area Process Created\n");
        write(1, buf, strlen(buf));
        ellipse(atoi(argv[3]), atoi(argv[4]), atoi(argv[5]));
        exit(0);
    }

    if ((pid4 = fork()) == 0) {
        sprintf(buf, "Pinball Process Created\n");
        write(1, buf, strlen(buf));
        pinballGame(atoi(argv[6]), atoi(argv[7]));
        exit(0);
    }

    /* waiting... */
    sprintf(buf, "Main Process Waits\n");
    write(1, buf, strlen(buf));
    for (i = 0; i < 4; ++i)
        wait(&status);

    sprintf(buf, "Main Process Exits\n");
    write(1, buf, strlen(buf));


    return 0;
}
