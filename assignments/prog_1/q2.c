#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

void main(int argc, char **argv)
{
     int i, n = atoi(argv[1]);

     for (i = 0; i < n; i++)
          if (fork() <= 0)
               break;
     printf("Process %ld with parent %ld\n", getpid(), getppid());
     sleep(1);
}
