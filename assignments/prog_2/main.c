/* ----------------------------------------------------------- */
/* NAME : 簡少澤                             User ID: 105820049 */
/* DUE DATE : 03/30/2020                                       */
/* PROGRAM ASSIGNMENT # 2                                      */
/* FILE NAME : main.c                                          */
/* PROGRAM PURPOSE :                                           */
/*    Read from IO and fork to execute merge sort              */
/* ----------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define BUFFER_SIZE         1024
#define INPUT_TOKEN_SIZE    128

typedef int bool;
#define true     1
#define false    0

/* ----------------------------------------------------------- */
/* FUNCTION  createSharedMemory :                              */
/*     Create 2 shared memory for storing elements and its     */
/*     index                                                   */
/* PARAMETER USAGE :                                           */
/*    shmId1 - Pointer to the shared memory id 1               */
/*    shmId2 - Pointer to the shared memory id 2               */
/*    size - The length of array that should be allocated      */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void createSharedMemory(int * shmId1, int * shmId2, int size)
{
    char printBuffer[BUFFER_SIZE];
    key_t key_1 = ftok("./", 'a');
    key_t key_2 = ftok("./", 'b');

    sprintf(printBuffer, "*** MAIN: shared memory key = %d\n", key_1);
    write(1, printBuffer, strlen(printBuffer));
    sprintf(printBuffer, "*** MAIN: shared memory key = %d\n", key_2);
    write(1, printBuffer, strlen(printBuffer));

    *shmId1 = shmget(key_1, sizeof(int) * size, IPC_CREAT | 0666);
    sprintf(printBuffer, "*** MAIN: shared memory created\n");
    write(1, printBuffer, strlen(printBuffer));

    *shmId2 = shmget(key_2, sizeof(int) * size, IPC_CREAT | 0666);
    sprintf(printBuffer, "*** MAIN: shared memory created\n");
    write(1, printBuffer, strlen(printBuffer));
}

/* ----------------------------------------------------------- */
/* FUNCTION  removeSharedMemory :                              */
/*     Remove the shared memory                                */
/* PARAMETER USAGE :                                           */
/*    shmId1 - Shared memory id 1                              */
/*    shmId2 - Shared memory id 2                              */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void removeSharedMemory(int shmId1, int shmId2)
{
    char printBuffer[BUFFER_SIZE];

    shmctl(shmId1, IPC_RMID, NULL);
    sprintf(printBuffer,
            "*** MAIN: input storing shared memory successfully removed\n");
    write(1, printBuffer, strlen(printBuffer));

    shmctl(shmId2, IPC_RMID, NULL);
    sprintf(printBuffer,
            "*** MAIN: temp storing shared memory successfully removed\n");
    write(1, printBuffer, strlen(printBuffer));
}

/* ----------------------------------------------------------- */
/* FUNCTION  strToTokens :                                     */
/*     Convert a string to an array of integers                */
/* PARAMETER USAGE :                                           */
/*    token - output array of integers                         */
/*    str - input string, tokens should be seperated by spaces */
/*    inputSize - size of the tokens, size of token should be  */
/*        greater than inputSize                               */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void strToTokens(int * token, char * str, int inputSize)
{
    int i;
    /* read the first token */
    char * pch = strtok(str, " \n");
    token[0] = atoi(pch);
    /* proceed to the read the rest of the string */
    for (i = 1; i < inputSize && pch != NULL; ++i) {
        pch = strtok(NULL, " \n");
        token[i] = atoi(pch);
    }
}

/* ----------------------------------------------------------- */
/* FUNCTION  elementToString :                                 */
/*     convert element to string with specified bound (range)  */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    size - the size of elements                              */
/*    output - output string                                   */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void elementToString(int * elements, int size, char * output)
{
    int i;
    char buf[10];
    strcpy(output, "");
    for (i = 0; i < size; ++i) {
        sprintf(buf, "   %d", elements[i]);
        strncat(output, buf, strlen(buf));
    }
    strncat(output, "\n", 1);
}

/* ----------------------------------------------------------- */
/* FUNCTION  printSortResult :                                 */
/*     Print the sorted result                                 */
/* PARAMETER USAGE :                                           */
/*    elements - the sorted array to print                     */
/*    size - the size of the sorted array                      */
/* FUNCTION CALLED :                                           */
/*    elementToString                                          */
/* ----------------------------------------------------------- */
void printSortResult(int * elements, int size)
{
    char buf[BUFFER_SIZE], numBuf[BUFFER_SIZE];
    sprintf(buf, "*** MAIN: merged array:\n   ");

    elementToString(elements, size, numBuf);

    strncat(buf, numBuf, strlen(numBuf));
    strncat(buf, "\n", 1);
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  main :                                            */
/*     main program. Creates other child processes.            */
/* PARAMETER USAGE :                                           */
/*    argc - argc                                              */
/*    argv - argv                                              */
/* FUNCTION CALLED :                                           */
/*    createSharedMemory, strToTokens, elementToString,        */
/*    printSortResult, removeSharedMemory                      */
/* ----------------------------------------------------------- */
int main (int argc, char * argv[])
{
    int inputSize, status;
    char buffer[BUFFER_SIZE], printBuffer[BUFFER_SIZE];
    pid_t pid;

    int elementShmId, IndexShmId;
    int * elements;

    sprintf(printBuffer, "Merge Sort with Multiple Processes:\n\n");
    write(1, printBuffer, strlen(printBuffer));

    /* read from IO, gets the size of the array to be sorted */
    fgets(buffer, BUFFER_SIZE, stdin);
    inputSize = atoi(buffer);
    fgets(buffer, BUFFER_SIZE, stdin);

    /* create 2 shared memory */
    createSharedMemory(&elementShmId, &IndexShmId, inputSize);
    elements = (int *) shmat(elementShmId, NULL, 0);
    sprintf(
        printBuffer,
        "*** MAIN: input storing shared memory attached and is ready to use\n\n"
    );
    write(1, printBuffer, strlen(printBuffer));

    /* convert string to int array, stores it in shm */
    strToTokens(elements, buffer, inputSize);

    sprintf(printBuffer,
            "Input array for mergesort has %d elements:\n",
            inputSize);
    write(1, printBuffer, strlen(printBuffer));
    elementToString(elements, inputSize, printBuffer);
    write(1, printBuffer, strlen(printBuffer));

    /* merge argv */
    char argvBuf[10];
    char * mergeArgv[5];
    sprintf(argvBuf, "%d", inputSize - 1);
    mergeArgv[0] = "./merge";
    mergeArgv[1] = "0";
    mergeArgv[2] = argvBuf;
    mergeArgv[3] = "0";   /* parent merge flag */
    mergeArgv[4] = NULL;

    sprintf(printBuffer, "*** MAIN: about to spawn the merge sort process\n");
    write(1, printBuffer, strlen(printBuffer));

    /* fork, start merge sort */
    if ((pid = fork()) < 0) {
        sprintf(printBuffer, "fork() failed, exit...\n");
        write(1, printBuffer, strlen(printBuffer));
        exit(1);
    }
    else if (pid == 0) {    /* child process */
        if (execvp(mergeArgv[0], mergeArgv) < 0) {
            sprintf(printBuffer, "execvp() failed, exit...\n");
            write(1, printBuffer, strlen(printBuffer));
            exit(1);
        }
    }
    else {                  /* parent process */
        wait(&status);
    }

    /* merged :) print result */
    printSortResult(elements, inputSize);

    /* detach and remove shm */
    shmdt((void *) elements);
    sprintf(printBuffer,
            "*** MAIN: ELEMENT shared memory successfully detached\n");
    write(1, printBuffer, strlen(printBuffer));
    removeSharedMemory(elementShmId, IndexShmId);
    sprintf(printBuffer,"*** MAIN: exits\n");
    write(1, printBuffer, strlen(printBuffer));

    return 0;
}
