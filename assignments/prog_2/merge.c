/* ----------------------------------------------------------- */
/* NAME : 簡少澤                             User ID: 105820049 */
/* DUE DATE : 03/30/2020                                       */
/* PROGRAM ASSIGNMENT # 2                                      */
/* FILE NAME : merge.c                                         */
/* PROGRAM PURPOSE :                                           */
/*    Perform merge sort                                       */
/* ----------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define BUFFER_SIZE         1024

typedef int bool;
#define true     1
#define false    0

/* ----------------------------------------------------------- */
/* FUNCTION  attachShm :                                       */
/*     Get key and attach to the shared memory                 */
/* PARAMETER USAGE :                                           */
/*    element - Pointer to the shared memory                   */
/*    index - Pointer to the shared memory                     */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void attachShm(int ** element, int ** index)
{
    char printBuffer[BUFFER_SIZE];
    key_t key_1 = ftok("./", 'a');
    key_t key_2 = ftok("./", 'b');

    int shmId1, shmId2;

    shmId1 = shmget(key_1, sizeof(int) * 8, 0666);
    shmId2 = shmget(key_2, sizeof(int) * 8, 0666);

    *element = (int *)shmat(shmId1, NULL, 0);

    sprintf(printBuffer, "   *** MERGE: shared memory attached and is ready "
            "to use for storing input elements purpose.\n");
    write(1, printBuffer, strlen(printBuffer));

    *index = (int *)shmat(shmId2, NULL, 0);

    sprintf(printBuffer, "   *** MERGE: shared memory attached and is ready "
            "to use for temp storing purpose.\n");
    write(1, printBuffer, strlen(printBuffer));
}

/* ----------------------------------------------------------- */
/* FUNCTION  detachShm :                                       */
/*     detatach to the shared memory                           */
/* PARAMETER USAGE :                                           */
/*    element - Pointer to the shared memory                   */
/*    index - Pointer to the shared memory                     */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void detachShm(int * elements, int * index)
{
    char printBuffer[BUFFER_SIZE];

    shmdt(elements);
    sprintf(printBuffer, "   *** MERGE: input storing shared memory\
 successfully detached\n");
    write(1, printBuffer, strlen(printBuffer));

    shmdt(index);
    sprintf(printBuffer, "   *** MERGE: temp storing shared memory\
 successfully detached\n");
    write(1, printBuffer, strlen(printBuffer));
}

/* ----------------------------------------------------------- */
/* FUNCTION  getSplitChildArgv :                               */
/*     generate argv for child process                         */
/* PARAMETER USAGE :                                           */
/*    argv - stores arguments for child process                */
/*    lbuf - string buffer for argv                            */
/*    rbuf - string buffer for argv                            */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void getSplitChildArgv(char * argv[],
                       char * lBuf,
                       char * rBuf,
                       int left,
                       int right)
{
    argv[0] = "./merge";
    sprintf(lBuf, "%d", left);
    argv[1] = lBuf;
    sprintf(rBuf, "%d", right);
    argv[2] = rBuf;
    argv[3] = "1";  /* additional flag to indicate "isChild" process */
    argv[4] = NULL;
}

/* ----------------------------------------------------------- */
/* FUNCTION  elementToString :                                 */
/*     convert element to string with specified bound (range)  */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/*    output - output string                                   */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void elementToString(int * elements, int left, int right, char * output)
{
    int i;
    char buf[10];
    strcpy(output, ""); /* clean the output to prevent unexcepted results */
    for (i = left; i <= right; ++i) {
        sprintf(buf, "   %d", elements[i]);
        strncat(output, buf, strlen(buf));
    }
    strncat(output, "\n", 1);
}

/* ----------------------------------------------------------- */
/* FUNCTION  printInputArray :                                 */
/*     print the entered array of this process                 */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/*    isChild - is this process forked by merge? If is a child */
/*        of merge, print something a bit different            */
/* FUNCTION CALLED :                                           */
/*    elementToString                                          */
/* ----------------------------------------------------------- */
void printInputArray(int * elements, int left, int right, bool isChild)
{
    char buf[BUFFER_SIZE], elementBuf[BUFFER_SIZE];

    if (isChild) {
        sprintf(buf,
            "   ### M-PROC(%d) created by M-PROC(%d): "
            "entering with a[%d..%d]\n   ",
            getpid(), getppid(), left, right);
    }
    else {              /* merge parent */
        sprintf(buf,
                "   ### M-PROC(%d): entering with a[%d..%d]\n   ",
                getpid(), left, right);
    }

    elementToString(elements, left, right, elementBuf);
    strncat(buf, elementBuf, strlen(elementBuf));
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  printCompletedArray :                             */
/*     print sorted array                                      */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/*    isChild - is this process forked by merge? If is a child */
/*        of merge, print something a bit different            */
/* FUNCTION CALLED :                                           */
/*    elementToString                                          */
/* ----------------------------------------------------------- */
void printCompletedArray(int * elements, int left, int right, bool isChild)
{
    char buf[BUFFER_SIZE], elementBuf[BUFFER_SIZE];

    if (isChild) {
        sprintf(buf,
            "   ### M-PROC(%d) created by M-PROC(%d): "
            "merge sort a[%d..%d] completed:\n   ",
            getpid(), getppid(), left, right);
    }
    else {              /* merge parent */
        sprintf(buf,
                "   ### M-PROC(%d): entering with a[%d..%d] completed:\n   ",
                getpid(), left, right);
    }

    elementToString(elements, left, right, elementBuf);
    strncat(buf, elementBuf, strlen(elementBuf));
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  splitBottomSwap :                                 */
/*     Compare two elements and swap if necessary. Stores the  */
/*     sorted element index in index shm                       */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void splitBottomSwap(int * elements, int left, int right)
{
    char printBuf[BUFFER_SIZE];
    char buf[BUFFER_SIZE];
    int temp;

    strcpy(printBuf, "");
    /* swapping index */
    if (elements[left] > elements[right]) {
        temp = elements[left];
        elements[left] = elements[right];
        elements[right] = temp;
    }

    sprintf(buf, "   ### M-PROC(%d) created by M-PROC(%d): "
            "entering with a[%d..%d] -- sorted\n",
            getpid(), getppid(), left, right);
    strncat(printBuf, buf, strlen(buf));
    sprintf(buf, "      %d   %d\n",
            elements[left], elements[right]);
    strncat(printBuf, buf, strlen(buf));
    write(1, printBuf, strlen(printBuf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  binarySearch :                                    */
/*     A modified binary search to find index                  */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    index - index of number to be searched                   */
/*    offset - index of n in x                                 */
/*    left - left bound of merge sort                          */
/*    right - right bound of merge sort                        */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
int binarySearch(int * elements, int index, int offset, int left, int right)
{
    int start = left;
    int end = right;
    int n = elements[index];
    char buf[BUFFER_SIZE];

    while (end >= start) {
        int middle = (end + start) / 2;
        if (n > elements[middle] && n < elements[middle + 1]) {/* found it */
            /* - left is to get the offset of the searched array */
            int tempIndex = middle + offset - left + 1;
            sprintf(buf, "      $$$ B-PROC(%d): a[%d] = %d is between "
                    "a[%d] = %d and a[%d] = %d and is written to temp[%d]\n",
                    getpid(),
                    index, n,
                    middle, elements[middle],
                    middle+1, elements[middle+1],
                    tempIndex);
            write(1, buf, strlen(buf));
            return tempIndex;
        }
        else if (n < elements[middle]){
            end = middle - 1;
        }
        else {
            start = middle + 1;
        }
    }

    return -1;  /* no possible in this case */
}

/* ----------------------------------------------------------- */
/* FUNCTION  printBinaryMergeInfo :                            */
/*     Prints B-PROC messages when merging                     */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    nIndex - index of number to compare                      */
/*    tgtIndex - target index to compare                       */
/*    tempIndex - index of temp to store                       */
/*    greatSmall - n "greater" or "smaller" than tgt           */
/* FUNCTION CALLED :                                           */
/*    (none)                                                   */
/* ----------------------------------------------------------- */
void printBinaryMergeInfo(int * elements, int nIndex, int tgtIndex,
                          int tempIndex, char * greatSmall)
{
    char buf[BUFFER_SIZE];
    sprintf(buf, "      $$$ B-PROC(%d): a[%d] = %d is %s than a[%d] = %d "
            "and is written to temp[%d]\n",
            getpid(),
            nIndex, elements[nIndex],
            greatSmall,
            tgtIndex, elements[tgtIndex],
            tempIndex);
    write(1, buf, strlen(buf));
}

/* ----------------------------------------------------------- */
/* FUNCTION  merge :                                           */
/*     merge two sub array                                     */
/* PARAMETER USAGE :                                           */
/*    elements - the array to be sorted                        */
/*    index - the array to store final element index           */
/*    left - left bound of merge sort                          */
/*    middle - middle of left and right                        */
/*    right - right bound of merge sort                        */
/* FUNCTION CALLED :                                           */
/*    binarySearch, printBinaryMergeInfo                       */
/* ----------------------------------------------------------- */
void merge(int * elements, int * index, int left, int middle, int right)
{
    /* for e index left ... middle, merge e of index middle+1 ... right */
    char buf[BUFFER_SIZE];
    int i, tempIndex, status;
    pid_t pid;

    sprintf(buf, "   ### M-PROC(%d) created by M-PROC(%d): "
            "both array section sorted. start merging\n", getpid(), getppid());
    write(1, buf, strlen(buf));

    /* for each element, fork and do binary search */
    for (i = left; i <= middle; ++i) {  /* x */
        if ((pid = fork()) == 0) {      /* child */
            sprintf(buf,
                    "      $$$ B-PROC(%d): created by M-PROC(%d) "
                    "for a[%d] = %d is created\n",
                    getpid(), getppid(), i, elements[i]);
            write(1, buf, strlen(buf));
            if (elements[i] < elements[middle + 1]) {
                tempIndex = i;
                printBinaryMergeInfo(
                    elements, i, middle+1, tempIndex, "smaller"
                );
            }
            else if (elements[i] > elements[right]) {
                tempIndex = right;
                printBinaryMergeInfo(
                    elements, i, right, tempIndex, "greater"
                );
            }
            else {
                tempIndex = left + binarySearch(elements, i,
                                        i-left, middle+1, right);
            }
            index[tempIndex] = elements[i];
            exit(0);
        }
    }
    for (i = middle + 1; i <= right; ++i) { /* y */
        if ((pid = fork()) == 0) {
            if (elements[i] < elements[left]) {
                tempIndex = i - middle + left - 1;
                printBinaryMergeInfo(
                    elements, i, left, tempIndex, "smaller"
                );
            }
            else if (elements[i] > elements[middle]) {
                tempIndex = i;
                printBinaryMergeInfo(
                    elements, i, middle, tempIndex, "greater"
                );
            }
            else {
                tempIndex = left + binarySearch(
                    elements, i, i - middle - 1, left, middle);
            }
            index[tempIndex] = elements[i];
            exit(0);
        }
    }

    for (i = 0; i < right - left + 1; ++i)
        wait(&status);

    /* write back to element */
    for (i = left; i <= right; ++i) {
        elements[i] = index[i];
    }
}

/* ----------------------------------------------------------- */
/* FUNCTION  main :                                            */
/*     merge main program. create child merge process to split */
/*     and merge                                               */
/* PARAMETER USAGE :                                           */
/*    argc - argc                                              */
/*    argv - Stores left, right and a flag isChild indicate    */
/*           if the process is a child process of merge        */
/* FUNCTION CALLED :                                           */
/*    attachShm, printInputArray, splitBottomSwap,             */
/*    getSplitChildArgv, merge, printCompletedArray, detachShm */
/* ----------------------------------------------------------- */
int main (int argc, char * argv[])
{
    char printBuffer[BUFFER_SIZE];
    int *elements, *index;
    int left, right, middle;
    int i, status;
    pid_t pidLeft, pidRight;
    bool isChild = atoi(argv[3]);

    attachShm(&elements, &index);

    left = atoi(argv[1]);
    right = atoi(argv[2]);
    middle = (right + left) / 2;

    printInputArray(elements, left, right, isChild);

    /* end condition, start writing to index */
    if ((right - left) <= 1) {
        splitBottomSwap(elements, left, right);
        exit(0);
    }

    char * childArgv[5];
    char leftbuf[10], rightBuf[10];

    if ((pidLeft = fork()) < 0) {
        sprintf(printBuffer, "fork() failed, exit...\n");
        write(1, printBuffer, strlen(printBuffer));
        exit(1);
    }
    /* left part of split */
    else if (pidLeft == 0) {
        getSplitChildArgv(childArgv, leftbuf, rightBuf, left, middle);
        if (execvp(childArgv[0], childArgv) < 0) {
            sprintf(printBuffer, "execvp() failed, exit...\n");
            write(1, printBuffer, strlen(printBuffer));
            exit(1);
        }
        /* child process will run to this line because of execvp */
    }
    else {
        /* parent, proceed to fork right part */
        if ((pidRight = fork()) < 0) {
            sprintf(printBuffer, "fork() failed, exit...\n");
            write(1, printBuffer, strlen(printBuffer));
            exit(1);
        }
        /* right part of split */
        else if (pidRight == 0) {
            getSplitChildArgv(childArgv, leftbuf, rightBuf, middle + 1, right);
            if (execvp(childArgv[0], childArgv) < 0) {
                sprintf(printBuffer, "execvp() failed, exit...\n");
                write(1, printBuffer, strlen(printBuffer));
                exit(1);
            }
            /* child process will run to this line because of execvp */
        }

        /*  wait for left and right process to finish */
        for (i = 0; i < 2; ++i)
            wait(&status);
    }

    /* merge process */
    merge(elements, index, left, middle, right);
    printCompletedArray(elements, left, right, isChild);

    detachShm(elements, index);
    return 0;
}
