// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/04/2020
// PROGRAM ASSIGNMENT # 3
// FILE NAME : thread.cpp
// PROGRAM PURPOSE :
//    Class implemention file for the PrefixSum class
// -----------------------------------------------------------
#include "thread.h"

// -----------------------------------------------------------
// FUNCTION  ThreadFunc :
//     Constructor of PrefixSum class
// PARAMETER USAGE :
//    - matrix: b matrix to store temporary result
//    - run: current run #
//    - n: index of the number to be computed
// FUNCTION CALLED :
//    (none)
// -----------------------------------------------------------
PrefixSum::PrefixSum(std::vector<int *> matrix, int run, int n)
          :matrix(matrix), run(run), n(n)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "prefix sum" << '\0';

    sprintf(printBuffer, "     Thread %d Created\n", n);
    write(1, printBuffer, strlen(printBuffer));
}

// -----------------------------------------------------------
// FUNCTION  ThreadFunc :
//     main of the thead, computes x[n]
// PARAMETER USAGE :
//    (none)
// FUNCTION CALLED :
//    (none)
// -----------------------------------------------------------
void PrefixSum::ThreadFunc()
{
    int gap = (int)pow(2, run - 1);
    matrix[run][n] = matrix[run - 1][n];

    if ((n - gap) < 0) {
        sprintf(printBuffer, "     Thread %d copies x[%d]\n", n, n);
        write(1, printBuffer, strlen(printBuffer));
    }
    else {
        sprintf(printBuffer, "     Thread %d computes x[%d] + x[%d-2^(%d-1)]\n",
                n, n, n, run);
        write(1, printBuffer, strlen(printBuffer));
        matrix[run][n] += matrix[run - 1][n - gap];
    }

    // exit...
    sprintf(printBuffer, "     Thread %d exits\n", n);
    write(1, printBuffer, strlen(printBuffer));
    Exit();
}
