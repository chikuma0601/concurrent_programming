// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/04/2020
// PROGRAM ASSIGNMENT # 3
// FILE NAME : thread.h
// PROGRAM PURPOSE :
//    Multi thread programming thread class for computing
//    prefix sum.
// -----------------------------------------------------------
#ifndef THREAD_H
#define THREAD_H

#include <cstdlib>
#include <cstring>
#include <cmath>

#include "ThreadClass.h"

#define BUFFER_SIZE 1024

class PrefixSum : public Thread
{
public:
    PrefixSum(std::vector<int *>, int, int);
private:
    void ThreadFunc();

    int run, n;
    char printBuffer[BUFFER_SIZE];
    std::vector<int *> matrix;
};

#endif
