// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/04/2020
// PROGRAM ASSIGNMENT # 3
// FILE NAME : thread-main.cpp
// PROGRAM PURPOSE :
//    Calculates Prefix sum in multi threaded method
// -----------------------------------------------------------
#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <cstring>

#define BUFFER_SIZE 1024

#include "thread.h"

// -----------------------------------------------------------
// FUNCTION  strToTokens :
//     Convert a string to an array of integers, returns a
//     std::vector containing tokens
// PARAMETER USAGE :
//    str - input string, tokens should be seperated by spaces
//    inputSize - size of the tokens, size of token should be
//        greater than inputSize
// FUNCTION CALLED :
//    (none)
// -----------------------------------------------------------
std::vector<int> strToTokens(char * str, int inputSize)
{
    std::vector<int> token;
    /* read the first token */
    char * pch = strtok(str, " \n");
    token.push_back(atoi(pch));
    /* proceed to the read the rest of the string */
    for (int i = 1; i < inputSize && pch != NULL; ++i) {
        pch = strtok(NULL, " \n");
        token.push_back(atoi(pch));
    }
    return token;
}

// -----------------------------------------------------------
// FUNCTION  createBMatrix :
//     Create the matrix for temporary storage in the process
//     of calculation. Returns a vector of int array.
// PARAMETER USAGE :
//    input - Input int array to initialize the first row of
//      the matrix and determine the size of the matrix.
// FUNCTION CALLED :
//    (none)
// -----------------------------------------------------------
std::vector<int *> createBMatrix(std::vector<int> input)
{
    std::vector<int *> matrix;
    int row = log(input.size()) / log(2) + 1;
    for (int i = 0; i < row; ++i)
        matrix.push_back(new int[input.size()]);
    // initialize the first row to input value
    for (int i = 0; i < input.size(); ++i)
        matrix[0][i] = input[i];

    return matrix;
}

// -----------------------------------------------------------
// FUNCTION  elementToString :
//     convert element to string with specified bound (range)
//     returns a standard string
// PARAMETER USAGE :
//    elements - the array to be sorted
//    size - the size of elements
// FUNCTION CALLED :
//    (none)
// -----------------------------------------------------------
std::string elementToString(int * elements, int size)
{
    std::string str;
    for (int i = 0; i < size; ++i) {
        str += std::string("    ") + std::to_string(elements[i]);
    }
    str += '\n';
    return str;
}

// -----------------------------------------------------------
// FUNCTION  main :
//     Main function, read from stdin and create threads
// PARAMETER USAGE :
//    - argc
//    - argv
// FUNCTION CALLED :
//    elementToString, createBMatrix, strToTokens
// -----------------------------------------------------------
int main(int argc, char * argv[])
{
    int inputSize;
    char buffer[BUFFER_SIZE], printBuffer[BUFFER_SIZE];

    sprintf(printBuffer, "Concurrent Prefix Sum Compuatation\n");
    write(1, printBuffer, strlen(printBuffer));

    // read input size from stdin
    fgets(buffer, BUFFER_SIZE, stdin);
    inputSize = atoi(buffer);
    sprintf(printBuffer, "Number of input data = %d\n", inputSize);
    write(1, printBuffer, strlen(printBuffer));

    fgets(buffer, BUFFER_SIZE, stdin);

    std::vector<int> tokens = strToTokens(buffer, inputSize);
    std::vector<int *> bMatrix = createBMatrix(tokens);

    sprintf(printBuffer, "Input array:\n");
    write(1, printBuffer, strlen(printBuffer));
    sprintf(printBuffer, elementToString(bMatrix[0], inputSize).c_str());
    write(1, printBuffer, strlen(printBuffer));
    sprintf(printBuffer, "\n\n");
    write(1, printBuffer, strlen(printBuffer));

    // calcuation
    for (int i = 1; i < bMatrix.size(); ++i) {
        sprintf(printBuffer, "Run %d\n", i);
        write(1, printBuffer, strlen(printBuffer));

        // create threads
        std::vector<PrefixSum *> threads;
        for (int n = 0; n < inputSize; ++n) {
            threads.push_back(new PrefixSum(bMatrix, i, n));
            threads[n]->Begin();
        }

        // join
        for (int n = 0; n < inputSize; ++n) {
            threads[n]->Join();
        }

        sprintf(printBuffer, "Result after run %d:\n", i);
        write(1, printBuffer, strlen(printBuffer));
        sprintf(printBuffer, elementToString(bMatrix[i], inputSize).c_str());
        write(1, printBuffer, strlen(printBuffer));

        for (int n = 0; n < inputSize; ++n)
            delete threads[n];
    }

    sprintf(printBuffer, "\n\nFinal result after run k:\n");
    write(1, printBuffer, strlen(printBuffer));
    sprintf(printBuffer,
        elementToString(
            bMatrix[bMatrix.size() - 1],
            inputSize
        ).c_str()
    );
    write(1, printBuffer, strlen(printBuffer));

    // delete allocated memory
    for (int i = 0; i < bMatrix.size(); ++i)
        delete [] bMatrix[i];

    Exit();

    return 0;
}
