// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 06/08/2020
// PROGRAM ASSIGNMENT # 5
// FILE NAME : thread-main.cpp
// PROGRAM PURPOSE :
//    River crossing using monitor
// -----------------------------------------------------------
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "thread.h"

#define BUFFER 1024

// -----------------------------------------------------------
// FUNCTION  main :
//     Main program
// PARAMETER USAGE :
//    argc, argv
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
int main (int argc, char * argv[])
{
    char buf[BUFFER];

    // default values
    const int cannibalCount = (atoi(argv[1]) == 0) ? 8 : atoi(argv[1]);
    const int missionaryCount = (atoi(argv[2]) == 0) ? 8 : atoi(argv[2]);
    const int boatRides = (atoi(argv[3]) == 0) ? 5 : atoi(argv[3]);

    std::vector<Cannibal *> cannibals;
    std::vector<Missionary *> missionaries;
    Boat boat(boatRides);

    // create thered
    for (int i = 0; i < cannibalCount; ++i)
        cannibals.push_back(new Cannibal(i+1));
    for (int i = 0; i < missionaryCount; ++i)
        missionaries.push_back(new Missionary(i+1));

    // start crossing the river
    for (int i = 0; i < cannibalCount; ++i)
        cannibals[i]->Begin();
    for (int i = 0; i < missionaryCount; ++i)
        missionaries[i]->Begin();
    boat.Begin();

    // join, wait for monitor to terminate this program
    for (int i = 0; i < cannibalCount; ++i)
        cannibals[i]->Join();
    for (int i = 0; i < missionaryCount; ++i)
        missionaries[i]->Join();
    boat.Join();

    Exit();
}
