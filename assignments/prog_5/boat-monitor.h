// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 06/08/2020
// PROGRAM ASSIGNMENT # 5
// FILE NAME : boat-monitor.h
// PROGRAM PURPOSE :
//    definition boat monitor procedures
// -----------------------------------------------------------
#ifndef BOAT_MONITOR_H
#define BOAT_MONITOR_H

#include <vector>
#include <algorithm>
#include <cstring>
#include <time.h>

#include "ThreadClass.h"

#define BUFFER 1024

enum BoatStatus {BOARDING, CROSSING};
struct Passenger {
    char type;
    char id;
};

class BoatMonitor: public Monitor
{
public:
    BoatMonitor(char *);
    void CannibalArrives(int);
    void MissionaryArrives(int);
    void BoatReady(int);
    void BoatDone();
    void Finish(int);

private:
    void board(int);
    void generateRandomSelectSeq(int *);
    bool isBoatAvaliable();
    const char * addHeadSpace (std::string, int);

    Condition missionaryQueue;
    Condition cannibalQueue;
    Condition boatWait;       // wait for passenger all aborad
    Condition crossingRiver;  // the boat is crossing, no one should leave the boat

    int waitingCount = 0;
    int cannibalWaitingCount = 0;   // for boarding decision
    int missionaryWaitingCount = 0;
    int passengerCount = 0;
    Passenger passengerInfo[3];

    BoatStatus boatStatus = BOARDING;
    char buf[BUFFER];
};

#endif
