// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 06/08/2020
// PROGRAM ASSIGNMENT # 5
// FILE NAME : boat-monitor.cpp
// PROGRAM PURPOSE :
//    implements boat monitor procedures
// -----------------------------------------------------------
#include "boat-monitor.h"

// -----------------------------------------------------------
// FUNCTION  BoatMonitor :
//     BoatMonitor initializer, initialize conditions and setup
//     random.
// PARAMETER USAGE :
//    name - monitor name
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
BoatMonitor::BoatMonitor(char * name)
    : Monitor(name, HOARE), missionaryQueue("misQueue"),
    cannibalQueue("canQueue"), boatWait("boatWait"), crossingRiver("crossing")
{
    srand(time(NULL));
}

// -----------------------------------------------------------
// FUNCTION  CannibalArrives :
//     cannibal arrived, wait in queue, if signaled wait in
//     boat until crossed.
// PARAMETER USAGE :
//    id - cannibal id to be recorded in passengerInfo
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::CannibalArrives(int id)
{
    MonitorBegin();

    if (isBoatAvaliable()) {
        ++waitingCount;
        ++cannibalWaitingCount;
        cannibalQueue.Wait();
        --cannibalWaitingCount;
        --waitingCount;
    }

    passengerInfo[passengerCount] = {'c', id};
    ++passengerCount;
    if (passengerCount == 3)    // all abroad! signal the boat
        boatWait.Signal();
    crossingRiver.Wait();

    MonitorEnd();
}

// -----------------------------------------------------------
// FUNCTION  MissionaryArrives :
//     Missionary arrived, wait in queue, if signaled wait in
//     boat until crossed.
// PARAMETER USAGE :
//    id - missionary id to be recorded in passengerInfo
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::MissionaryArrives(int id)
{
    MonitorBegin();

    if (isBoatAvaliable()) { // if unable to board, wait
        ++waitingCount;
        ++missionaryWaitingCount;
        missionaryQueue.Wait();
        --missionaryWaitingCount;
        --waitingCount;
    }

    passengerInfo[passengerCount] = {'m', id};
    ++passengerCount;
    if (passengerCount == 3)    // all abroad! signal the boat
        boatWait.Signal();
    crossingRiver.Wait();

    MonitorEnd();
}

// -----------------------------------------------------------
// FUNCTION  BoatReady :
//     The boat is ready, load passesngers and start crossing
// PARAMETER USAGE :
//    ride - the current ride number
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::BoatReady(int ride)
{
    MonitorBegin();

    sprintf(buf, "***** The boat is ready\n");
    write(1, buf, strlen(buf));

    // release
    board(ride);

    // If not enough passenger waiting to cross
    if (passengerCount < 3) {
        boatStatus = BOARDING;
        boatWait.Wait();
    }
    boatStatus = CROSSING;

    // print here due to information might be changed after leaving monitor
    sprintf(buf, "***** Boat load (%d): Passenger list (%c%d, %c%d, %c%d)\n",
            ride, passengerInfo[0].type, passengerInfo[0].id,
            passengerInfo[1].type, passengerInfo[1].id,
            passengerInfo[2].type, passengerInfo[2].id);
    write(1, buf, strlen(buf));

    MonitorEnd();
}

// -----------------------------------------------------------
// FUNCTION  BoatDone :
//     The boat arrived, signal the passengers to make them
//     leave the boat.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::BoatDone()
{
    MonitorBegin();
    passengerCount = 0;
    for (int i = 0; i < 3; ++i)
        crossingRiver.Signal();
    MonitorEnd();
}

// -----------------------------------------------------------
// FUNCTION  board :
//     Randomly board passenger, but board safely.
// PARAMETER USAGE :
//    ride - The current ride count
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::board(int ride)
{
    int seq[3] = {0, 1, 2};
    generateRandomSelectSeq(seq);
    // 2m1c, 3m, 3c
    // try to load passenger, if unavailable, try another
    // kind of stupid, but it's correct and I can't think of a better way
    for (int i = 0; i < 3; ++i) {
        if (seq[i] == 0) {  // 2m1c
            if (cannibalWaitingCount >= 1 && missionaryWaitingCount >= 2) {
                cannibalQueue.Signal();     // fill in the passenger info
                missionaryQueue.Signal();   // and cross using the boat
                missionaryQueue.Signal();
                sprintf(buf, "MONITOR(%d): one cannibal (%d) and two "
                        "missionaries (%d, %d) are selected\n", ride + 1,
                        passengerInfo[0].id,
                        passengerInfo[1].id,
                        passengerInfo[2].id);
                write(1, buf, strlen(buf));
                return;
            }
        }
        else if (seq[i] == 1) {
            if (missionaryWaitingCount >= 3) {
                missionaryQueue.Signal();
                missionaryQueue.Signal();
                missionaryQueue.Signal();
                sprintf(buf, "MONITOR(%d): three missionaries (%d, %d, %d) are "
                        "selected\n", ride + 1,
                        passengerInfo[0].id,
                        passengerInfo[1].id,
                        passengerInfo[2].id);
                write(1, buf, strlen(buf));
                return;
            }
        }
        else if (seq[i] == 2) {
            if (cannibalWaitingCount >= 3) {
                cannibalQueue.Signal();
                cannibalQueue.Signal();
                cannibalQueue.Signal();
                sprintf(buf, "MONITOR(%d): three cannibals (%d, %d, %d) are "
                        "selected\n", ride + 1,
                        passengerInfo[0].id,
                        passengerInfo[1].id,
                        passengerInfo[2].id);
                write(1, buf, strlen(buf));
                return;
            }
        }
    }
}

// -----------------------------------------------------------
// FUNCTION  Finish :
//     River cross is closed, exit the program
// PARAMETER USAGE :
//    ride - Total rides(crosses) had been made
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::Finish(int ride) {
    sprintf(buf, "MONITOR: %d crosses have been made.\n", ride);
    write(1, buf, strlen(buf));
    sprintf(buf, "MONITOR: This river cross is closed indefinitely for "
            "renovation.\n");
    write(1, buf, strlen(buf));

    exit(0);    // :/
}

// -----------------------------------------------------------
// FUNCTION  generateRandomSelectSeq :
//     Randomly creates a sequence to try load passengers
//     0 = 2m1c, 1 = 3m, 2 = 3c
// PARAMETER USAGE :
//    ride - The current ride count
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BoatMonitor::generateRandomSelectSeq(int * seq)
{
    for (int i = 0; i < 3; ++i) {
        int temp = seq[i];
        int newIndex = rand() % 3;
        seq[i] = seq[newIndex];
        seq[newIndex] = temp;
    }
}

// -----------------------------------------------------------
// FUNCTION  isBoatAvaliable :
//     Check if there are empty space on boat and if the boat
//     is crossing the river.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
bool BoatMonitor::isBoatAvaliable()
{
    return (passengerCount < 3 || boatStatus != CROSSING)? true: false;
}

// -----------------------------------------------------------
// FUNCTION  addHeadSpace
//     add spaces at output string
// PARAMETER USAGE :
//    - str: string to add space at head
//    - id: id of the object, starts on i th column
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
const char * BoatMonitor::addHeadSpace (std::string str, int id)
{
    // missionary / cannibal i starts on column i
    for (int i = 0; i < id - 1; ++i)
        str = " " + str;
    return str.c_str();
}
