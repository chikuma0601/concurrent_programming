// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 06/08/2020
// PROGRAM ASSIGNMENT # 5
// FILE NAME : thread.cpp
// PROGRAM PURPOSE :
//    implements boat, cannibal and missionary thread
// -----------------------------------------------------------
#include "thread.h"

static BoatMonitor monitor("boat monitor");

// -----------------------------------------------------------
// FUNCTION  addHeadSpace :
//     add spaces at output string
// PARAMETER USAGE :
//    - str: string to add space at head
//    - id: id of the object, starts on i th column
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
const char * addHeadSpace (std::string str, int id)
{
    // missionary / cannibal i starts on column i
    for (int i = 0; i < id - 1; ++i)
        str = " " + str;
    return str.c_str();
}

// -----------------------------------------------------------
// FUNCTION  Cannibal::Cannibal :
//     Cannibal thread contructor
// PARAMETER USAGE :
//    - id: id of the cannibal
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
Cannibal::Cannibal(int id): id(id)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "cannibal_" << id << '\0';
}

// -----------------------------------------------------------
// FUNCTION  Cannibal::ThreadFunc :
//     Main program of cannibal, does not eat other people but
//     cross a river.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Cannibal::ThreadFunc()
{
    sprintf(buf, addHeadSpace("Cannibal %d starts\n", id), id);
    write(1, buf, strlen(buf));

    while(1) {
        randomDelay();
        sprintf(buf, addHeadSpace("Cannibal %d arrives\n", id), id);
        write(1, buf, strlen(buf));
        monitor.CannibalArrives(id);
    }

    Exit();
}

// -----------------------------------------------------------
// FUNCTION  Cannibal::randomDelay :
//     Delay for some time (randomly)
//     This is copied everywhere due to that Delay is a method
//     of Thread class. Maybe it is better make a base class
//     out of it.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Cannibal::randomDelay ()
{
    srand(time(NULL));
    int randomNum = rand() % 10;
    for (int i = 0; i < randomNum; ++i)
        Delay();
}

// -----------------------------------------------------------
// FUNCTION  Missionary::Missionary :
//     Missionary thread constructor
// PARAMETER USAGE :
//    id - id of the missionary
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
Missionary::Missionary(int id): id(id)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "missionary_" << id << '\0';
}

// -----------------------------------------------------------
// FUNCTION  Missionary::ThreadFunc :
//     main program of missionary, also cross the river by
//     using boat monitor.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Missionary::ThreadFunc()
{
    sprintf(buf, addHeadSpace("Missionary %d starts\n", id), id);
    write(1, buf, strlen(buf));

    while(1) {
        randomDelay();
        sprintf(buf, addHeadSpace("Missionary %d arrives\n", id), id);
        write(1, buf, strlen(buf));
        monitor.MissionaryArrives(id);
    }

    Exit();
}

// -----------------------------------------------------------
// FUNCTION  Missionary::randomDelay :
//     Delay for some time (randomly)
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Missionary::randomDelay ()
{
    srand(time(NULL));
    int randomNum = rand() % 10;
    for (int i = 0; i < randomNum; ++i)
        Delay();
}

// -----------------------------------------------------------
// FUNCTION  Boat::Boat :
//     Boat thread constructor
// PARAMETER USAGE :
//    load - number of this boat will serve
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
Boat::Boat(int load): load(load)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "boat" << '\0';
}

// -----------------------------------------------------------
// FUNCTION  Boat::Boat :
//     Boat thread main program, terminates after serving n times
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Boat::ThreadFunc()
{
    sprintf(buf, "***** BOAT thread starts\n");
    write(1, buf, strlen(buf));

    for (int i = 0; i < load; ++i) {
        Delay();
        monitor.BoatReady(i);

        sprintf(buf, "***** Boat load (%d): Completed\n", i + 1);
        write(1, buf, strlen(buf));

        Delay();
        monitor.BoatDone();
    }

    // terminate after the last boat crossed the river
    monitor.Finish(load);

    Exit(); // This line will not be executed
}

// -----------------------------------------------------------
// FUNCTION  Boat::randomDelay :
//     Delay for some time (randomly)
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void Boat::randomDelay ()
{
    srand(time(NULL));
    int randomNum = rand() % 10;
    for (int i = 0; i < randomNum; ++i)
        Delay();
}
