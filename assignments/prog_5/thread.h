// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 06/08/2020
// PROGRAM ASSIGNMENT # 5
// FILE NAME : thread.h
// PROGRAM PURPOSE :
//    definition of all threads' interface
// -----------------------------------------------------------
#ifndef THREAD_H
#define THREAD_H

#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "ThreadClass.h"
#include "boat-monitor.h"

#define BUFFER 1024

const char * addHeadSpace(std::string, int id);

class Cannibal: public Thread
{
public:
    Cannibal(int);
private:
    void ThreadFunc();
    void randomDelay();

    const int id;
    char buf[BUFFER];
};

class Missionary: public Thread
{
public:
    Missionary(int);
private:
    void ThreadFunc();
    void randomDelay();

    const int id;
    char buf[BUFFER];
};

class Boat: public Thread
{
public:
    Boat(int);

private:
    void ThreadFunc();
    void randomDelay();

    const int load;
    char buf[BUFFER];
};

#endif
