// ----------------------------------------------------------- 
// NAME : 簡少澤                             User ID: 105820049 
// DUE DATE : 06/22/2020                                      
// PROGRAM ASSIGNMENT #6                                        
// FILE NAME : thread.h           
// PROGRAM PURPOSE :                                           
//    Defines prime thread and master thread.     
// ----------------------------------------------------------- 
#ifndef THREAD_H
#define THREAD_H

#include <string>
#include <vector>
#include <cstring>

#include "ThreadClass.h"

#define BUFFER_SIZE 1024

const int NOT_DEFINED = -2;
const int END_OF_DATA = -1; //end of input flag

extern std::vector<int> primes;

class PrimeThread: public Thread
{
public:
	PrimeThread(int index, int threadID);
	~PrimeThread();
	SynOneToOneChannel *channel;
private:
	void ThreadFunc();
	const char * formatOutput(std::string str);
	char buf[BUFFER_SIZE];
	int Index;
	int Number;
	PrimeThread *neighbor;  // next sort thread
};

class MasterThread: public Thread
{
public:
	MasterThread(int threadID, int nun);
private:
	~MasterThread();
	void ThreadFunc();
	void printPrimes();
	PrimeThread * firstPrimeThread;
	char buf[BUFFER_SIZE];
	int num;
};

#endif
