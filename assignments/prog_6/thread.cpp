// ----------------------------------------------------------- 
// NAME : 簡少澤                             User ID: 105820049 
// DUE DATE : 06/22/2020                                      
// PROGRAM ASSIGNMENT #6                                        
// FILE NAME : thread.cpp           
// PROGRAM PURPOSE :                                           
//    Implementation of prime thread and master thread.
// ----------------------------------------------------------- 
#include "thread.h"

// ----------------------------------------------------------- 
// FUNCTION  PrimeThread::PrimeThread :                        
//    Constructor of prime thread, creates synchoronous channel
//    with user defined thread id.
// PARAMETER USAGE :                                           
//    str - string to be printed    
// FUNCTION CALLED :                                           
//    (none)        
// -----------------------------------------------------------
PrimeThread::PrimeThread (int index, int threadID)
    :Index(index), neighbor(NULL), Number(NOT_DEFINED)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "PrimeThread_" << threadID << '\0';

    UserDefinedThreadID = threadID; // cannot put in member initizlizer list ??
    char * ChannelName = "num_pass_channel";
    // initialize a channel between this and parent thread
    channel = new SynOneToOneChannel(ChannelName, threadID - 1, threadID);
}

// ----------------------------------------------------------- 
// FUNCTION  PrimeThread::~PrimeThread :                        
//    destructor of prime thread, delete allocated channel
// PARAMETER USAGE :                                           
//    (none)
// FUNCTION CALLED :                                           
//    (none)        
// -----------------------------------------------------------
PrimeThread::~PrimeThread()
{
    delete channel;
}

// ----------------------------------------------------------- 
// FUNCTION  PrimeThread::ThreadFunc :                        
//    Main function of prime thread, saves the first received 
//	  number and check if the following numbers could be divided
//    by itself. Terminated if receive EOD.
// PARAMETER USAGE :                                           
//    (none)   
// FUNCTION CALLED :                                           
//    formatOutput     
// -----------------------------------------------------------
void PrimeThread::ThreadFunc () 
{
    Thread::ThreadFunc();
    int number, tempNum;        // Temporary holdes the number received from
                                // channel.
    Thread_t self = GetID();    // GetID returns the id of the thread

    while (true) {
        channel->Receive(&number, sizeof(int)); // receive a number

        if (number == END_OF_DATA) {
            sprintf(buf, formatOutput("P%d receives END\n"),
                    UserDefinedThreadID);
            write(1, buf, strlen(buf));
            break;
        }
        else if (Number == NOT_DEFINED) {
            Number = number;
            sprintf(buf, formatOutput("P%d starts and memorize %d\n"),
                    UserDefinedThreadID, number);
            write(1, buf, strlen(buf));

            primes.push_back(Number);
        }
        else {
            if (number % Number != 0) {
                tempNum = number;               // seperate receice and send

                sprintf(buf, formatOutput("P%d receives %d\n"),
                        UserDefinedThreadID, number);
                write(1, buf, strlen(buf));

                if (neighbor == NULL) {
                    sprintf(buf, formatOutput("P%d creates P%d\n"),
                            UserDefinedThreadID, UserDefinedThreadID + 1);
                    write(1, buf, strlen(buf));
                    neighbor = new PrimeThread(Index + 1, UserDefinedThreadID + 1);
                    neighbor->Begin();
                }

                // send message to neighbor thread
                neighbor->channel->Send(&tempNum, sizeof(int));
            }
            else {
                sprintf(buf, formatOutput("P%d ignores %d\n"),
                        UserDefinedThreadID, number);
                write(1, buf, strlen(buf));
            }
        }
    }

    if (neighbor != NULL) { // received EOD, pass it down if neightbor exists
        neighbor->channel->Send(&number, sizeof(int));
        neighbor->Join();
    }

    Exit();
}

// ----------------------------------------------------------- 
// FUNCTION  PrimeThread::formatOutput :                        
//    Add indents to the string to be printed out by the user
//    defined thread id
// PARAMETER USAGE :                                           
//    str - string to be printed    
// FUNCTION CALLED :                                           
//    (none)        
// ----------------------------------------------------------- 
const char * PrimeThread::formatOutput(std::string str)
{
    // really simple way using std::string
    // first number is 2, starts @ column 3
    for (int i = 2; i <= UserDefinedThreadID; ++i)
        str = "  " + str;
    return str.c_str();
}

// ----------------------------------------------------------- 
// FUNCTION  MasterThread::MasterThread :                        
//    MasterThread constructor, saves theadID and the numbers
//    to be passed down.
// PARAMETER USAGE :                                           
//    num - primes between 2 and num     
// FUNCTION CALLED :                                           
//    (none)        
// ----------------------------------------------------------- 
MasterThread::MasterThread(int threadID, int num): num(num)
{
    UserDefinedThreadID = threadID;
    ThreadName.seekp(0, ios::beg);
    ThreadName << "MasterThread" << '\0';
}

// ----------------------------------------------------------- 
// FUNCTION  MasterThread::ThreadFunc :                        
//    Main function of master thread, creates P2 and send
//    numbers sequentially to channel.
// PARAMETER USAGE :                                           
//    (none)     
// FUNCTION CALLED :                                           
//    printPrimes        
// ----------------------------------------------------------- 
void MasterThread::ThreadFunc()
{
    Thread::ThreadFunc();

    firstPrimeThread = new PrimeThread(1, 2);
    firstPrimeThread->Begin();


    int input, endSignal = END_OF_DATA;
    for (int i = 2; i <= num; ++i) {
        firstPrimeThread->channel->Send(&i, sizeof(int));
    }

    sprintf(buf, "Master sends END\n");
    write(1, buf, strlen(buf));
    firstPrimeThread->channel->Send(&endSignal, sizeof(int));
    firstPrimeThread->Join();
    
    printPrimes();

    Exit();
}

// ----------------------------------------------------------- 
// FUNCTION  MasterThread::printPrimes :                        
//    Print the results in line
// PARAMETER USAGE :                                           
//    (none)     
// FUNCTION CALLED :                                           
//    (none)         
// ----------------------------------------------------------- 
void MasterThread::printPrimes()
{
    char temp[16];
    std::string str = "Master prints the complete result:\n  ";
    for (int i = 0; i < primes.size(); ++i) {
        sprintf(temp, "%d  ", primes[i]);
        str += std::string(temp);
    }
    str += "\n";
    sprintf(buf, str.c_str());
    write(1, buf, strlen(str.c_str()));
}

