// ----------------------------------------------------------- 
// NAME : 簡少澤                             User ID: 105820049 
// DUE DATE : 06/22/2020                                      
// PROGRAM ASSIGNMENT #6                                        
// FILE NAME : thread-main.cpp           
// PROGRAM PURPOSE :                                           
//    Find primes in a concurrent way using threads.       
// ----------------------------------------------------------- 
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "thread.h"

#define BUFFER 1024

std::vector<int> primes;

// ----------------------------------------------------------- 
// FUNCTION  main :                        
//     main program, reads from arguments and starts master thread.                           
// PARAMETER USAGE :                                           
//    argc, argv     
// FUNCTION CALLED :                                           
//    (none)         
// ----------------------------------------------------------- 
int main (int argc, char * argv[])
{
    char buf[BUFFER];

    if (argc < 2)
        Exit();
    int num = atoi(argv[1]);

    sprintf(buf, "Master starts\n");
    write(1, buf, strlen(buf));

    MasterThread * masterThread;
    
    masterThread = new MasterThread(1, num);
    masterThread->Begin();
    masterThread->Join();

    sprintf(buf, "Master terminates\n");
    write(1, buf, strlen(buf));

    Exit();
}

