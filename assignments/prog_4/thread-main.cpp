// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/25/2020
// PROGRAM ASSIGNMENT # 4
// FILE NAME : thread-main.cpp
// PROGRAM PURPOSE :
//    Hungry eagles game
// -----------------------------------------------------------
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "thread.h"

#define BUFFER 1024

Semaphore * nap;         // mother is sleeping or finding food
Semaphore * pot;         // count of avaiable pots
Semaphore * waitQueue;   // numbers of eagles ready to eat
Semaphore * checkQueue;  // for checking in one by one

Mutex foodLock;
Mutex retireMutex;

bool retired = false;
bool isFirst = true;
int foodCount = 0;
int waitingCount = 0;

// -----------------------------------------------------------
// FUNCTION  main :
//     Main function of the program
// PARAMETER USAGE :
//    argc, argv
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
int main (int argc, char * argv[])
{
    char buf[BUFFER];

    // read arguments
    // number of feeding pots, baby eagles, feedings
    const int m = (atoi(argv[1]) == 0) ? 10 : atoi(argv[1]);
    const int n = (atoi(argv[2]) == 0) ? 10 : atoi(argv[2]);
    const int t = (atoi(argv[3]) == 0) ? 10 : atoi(argv[3]);

    // initializing semaphores
    nap = new Semaphore("nap", 1);
    pot = new Semaphore("pot", m);
    waitQueue = new Semaphore("wait queue", 0);
    checkQueue = new Semaphore("check in queue", 1);

    MotherEagle motherEagle(m, n, t);
    std::vector<BabyEagle*> babyEagles;
    for (int i = 0; i < n; ++i)
        babyEagles.push_back(new BabyEagle(i + 1));

    sprintf(buf, "MAIN: There are %d baby eagles, %d feeding pots, and %d "
            "feedings.\n", n, m, t);
    write(1, buf, strlen(buf));
    sprintf(buf, "MAIN: Game starts!!!!!\n");
    write(1, buf, strlen(buf));

    motherEagle.Begin();
    for (int i = 0; i < n; ++i)
        babyEagles[i]->Begin();


    motherEagle.Join();
    for (int i = 0; i < n; ++i)
        babyEagles[i]->Join();

    sprintf(buf, "Mother eagle retires after serving %d feedings. "
            "Game ends!!!\n", t);
    write(1, buf, strlen(buf));
    Exit();
}
