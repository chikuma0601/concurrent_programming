// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/25/2020
// PROGRAM ASSIGNMENT # 4
// FILE NAME : thread-support.cpp
// PROGRAM PURPOSE :
//    implements methods that signals other threads
// -----------------------------------------------------------
#include "thread.h"

// -----------------------------------------------------------
// FUNCTION  MotherEagle::goto_sleep :
//     Goes to sleep. Sleep until someone wakes her up.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void MotherEagle::goto_sleep ()
{
    nap->Wait();
}

// -----------------------------------------------------------
// FUNCTION  MotherEagle::food_ready :
//     Food is ready, signals all waiting eagles.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void MotherEagle::food_ready ()
{
    foodLock.Lock();
        foodCount = pot;
        isFirst = true;
    foodLock.Unlock();

    int watingEagle = waitingCount;
    for (int i = 0; i < watingEagle; ++i) {
        --waitingCount;
        waitQueue->Signal();
    }

}

// -----------------------------------------------------------
// FUNCTION  BabyEagle::ready_to_eat :
//     Baby eagle is ready to eat, wait for mother prepare
//     for feedings if there are none. Wakes motherEagle up
//     if she is sleeping and there are nothing to eat.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    addHeadSpace
// -----------------------------------------------------------
void BabyEagle::ready_to_eat ()
{
    sprintf(buf, addHeadSpace("Baby eagle %d is ready to eat.\n"), id);
    write(1, buf, strlen(buf));

    while(!retired) {       // will need to check again if waited for feeding
        checkQueue->Wait(); // check in one by one
        if (foodCount != 0) {    // there are still some food, eat it!
            sprintf(buf, addHeadSpace("Baby eagle %d is eating using feeding pot %d.\n"),
                    id, foodCount);
            write(1, buf, strlen(buf));
            foodCount--;
            checkQueue->Signal();
            pot->Wait();
            break;          // proceed to eat
        }
        else if (!retired && foodCount == 0) {
            if (isFirst) {      // if first
                nap->Signal();  // wake up mother
                isFirst = false;
                sprintf(buf, " Mother eagle is awoke by baby eagle %d and starts"
                        " preparing food.\n", id);
                write(1, buf, strlen(buf));
            }

            ++waitingCount;

            checkQueue->Signal(); // note to self: possible race condition beyond this point
            waitQueue->Wait();    // wait for feedings
        }
        else if (retired && foodCount == 0) {
            checkQueue->Signal();
            Exit();
        }
    }
}

// -----------------------------------------------------------
// FUNCTION  BabyEagle::finish_eating :
//     Finishes eating, release the pot.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    addHeadSpace
// -----------------------------------------------------------
void BabyEagle::finish_eating ()
{
    sprintf(buf, addHeadSpace("Baby eagle %d finishes eating.\n"), id);
    write(1, buf, strlen(buf));
    pot->Signal();
    if (retired)
        Exit();
}
