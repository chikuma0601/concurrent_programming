// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/25/2020
// PROGRAM ASSIGNMENT # 4
// FILE NAME : thread.h
// PROGRAM PURPOSE :
//    Defines class MotherEagle, BabyEagle, semaphores, mutex
//    and other global variables that are shared between threads.
// -----------------------------------------------------------
#ifndef THREAD_H
#define THREAD_H

#include <cstdlib>
#include <cstring>
#include <unistd.h>

#include "ThreadClass.h"

#define BUFFER 1024

// Semaphores
extern Semaphore * nap;         // mother is sleeping or finding food
extern Semaphore * pot;         // count of avaiable pots
extern Semaphore * waitQueue;   // numbers of eagles ready to eat
extern Semaphore * checkQueue;  // for checking in one by one

extern Mutex foodLock;          // lock all pots
extern Mutex retireMutex;

extern bool retired;
extern bool isFirst;
extern int foodCount;           // number of feedings
extern int waitingCount;        // number of waiting eagles

class MotherEagle: public Thread
{
public:
    MotherEagle(int, int, int);
    void goto_sleep();
    void food_ready();
private:
    void ThreadFunc ();
    void randomDelay ();

    char buf[BUFFER];
    const int feed;
    const int pot;
    const int babyCount;
};

class BabyEagle: public Thread
{
public:
    BabyEagle (int);
    void ready_to_eat ();
    void finish_eating ();
private:
    void ThreadFunc ();
    void randomDelay ();
    const char * addHeadSpace (std::string);

    char buf[BUFFER];
    const int id;
};

#endif
