// -----------------------------------------------------------
// NAME : 簡少澤                         User ID: 105820049
// DUE DATE : 05/25/2020
// PROGRAM ASSIGNMENT # 4
// FILE NAME : thread.cpp
// PROGRAM PURPOSE :
//    implements thead's main methods of MotherEagle and BabyEagle
// -----------------------------------------------------------
#include "thread.h"

// -----------------------------------------------------------
// FUNCTION  MotherEagle::MotherEagle :
//     initializer of class MotherEagle
// PARAMETER USAGE :
//    - pot: number of feeding pots
//    - babyCount: number of babyEagles
//    - feed: t times to feed
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
MotherEagle::MotherEagle (int pot, int babyCount, int feed)
    :pot(pot), babyCount(babyCount), feed(feed)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "mother_eagle" << '\0';
}

// -----------------------------------------------------------
// FUNCTION  MotherEagle::ThreadFunc :
//     Main program of thread, Naps, find food and does other
//     things.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    randomDelay, goto_sleep, food_ready
// -----------------------------------------------------------
void MotherEagle::ThreadFunc ()
{
    sprintf(buf, "Mother eagle started.\n");
    write(1, buf, strlen(buf));

    for (int i = 0; i < feed; ++i) {
        sprintf(buf, "Mother eagle takes a nap.\n");
        write(1, buf, strlen(buf));
        goto_sleep();

        randomDelay();  // food prepare time
        sprintf(buf, "Mother eagle says \"Feeding (%d)\"\n", i + 1);
        write(1, buf, strlen(buf));
        food_ready();
        randomDelay();
    }

    retireMutex.Lock();
    retired = true;
    retireMutex.Unlock();

    // tell the kids "get lost" no more feedings!
    // only cares if I could get all out of waitQueue
    for (int i = 0; i < babyCount; ++i)
        waitQueue->Signal();

    Exit();
}

// -----------------------------------------------------------
// FUNCTION  MotherEagle::randomDelay :
//     Delay for some time (randomly)
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void MotherEagle::randomDelay ()
{
    srand(time(NULL));
    int randomNum = rand() % 10;
    for (int i = 0; i < randomNum; ++i)
        Delay();
}

BabyEagle::BabyEagle (int id): id(id)
{
    ThreadName.seekp(0, ios::beg);
    ThreadName << "baby_eagle_" << id << '\0';
}

// -----------------------------------------------------------
// FUNCTION  BabyEagle::ThreadFunc :
//     Main program of thread, Playes, eat and does other things.
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    randomDelay, ready_to_eat, finish_eating, addHeadSpace
// -----------------------------------------------------------
void BabyEagle::ThreadFunc ()
{
    sprintf(buf, addHeadSpace("Baby eagle %d started.\n"), id);
    write(1, buf, strlen(buf));

    while (!retired) {
        randomDelay();      // playing
        ready_to_eat();
        randomDelay();      // eating
        finish_eating();
    }

    Exit();
}

// -----------------------------------------------------------
// FUNCTION  BabyEagle::randomDelay :
//     Delay for some time (randomly)
// PARAMETER USAGE :
//    (None)
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
void BabyEagle::randomDelay ()
{
    srand(time(NULL));
    int randomNum = rand() % 10;
    for (int i = 0; i < randomNum; ++i)
        Delay();
}

// -----------------------------------------------------------
// FUNCTION  BabyEagle::randomDelay :
//     add spaces at output string
// PARAMETER USAGE :
//    - str: string to add space at head
// FUNCTION CALLED :
//    (None)
// -----------------------------------------------------------
const char * BabyEagle::addHeadSpace (std::string str)
{
    for (int i = 0; i < id; ++i)
        str = " " + str;
    return str.c_str();
}
